#include "header.h"

component *cursor = NULL;

int push(component *stackobj, int newData);
int pop(component *stackobj, int *Tx);

int push(component *stackobj, int newData){
   /* returns 0 if no memory */
   /* returns 1 if success */
   /* assert(stackobj != NULL);*/

   /* check if enough memory */
   if ((stackobj = malloc(sizeof(component))) == NULL){
      printf("Not enough memory to allocate!\n Exiting...");
      return 0;
   }

   /* if enough to allocate */
   else{
      stackobj->number = newData;			/* stores data in stack */

      /* check if empty new object */
      if (cursor == NULL){
	 stackobj->nextcomponent = NULL;
	 cursor = stackobj;
      }

      /* if existing object */
      else{
	 stackobj->nextcomponent = cursor;		/* links component with next */
	 cursor = stackobj;				/* cursor is linked to this component */
      }
      return 1;
   }
}

int pop(component *stackobj, int *Tx){
   /* returns 0 if nothing on stack */
   /* returns 1 if success */

   if (cursor == NULL){
      printf("Nothing stored on stack!\n");
      return 0;
   }

   else{
      stackobj = cursor;			/* points to new first component */
      cursor = stackobj->nextcomponent;		/* points to second component */
      *Tx = stackobj->number;			/* transfers from stack to variable */
      free(stackobj);				/* clears memory of first object */
      return 1;
   }
}

int main(int argc, char *argv[]){

   component *membank;
   int result[3];
   int i, *intPtr = NULL;

   result[0]=0;
   result[1]=1;
   result[2]=2;
   result[3]=3;

   for (i=0; i<3; i++){
      intPtr=&result[i];
      push(membank, *intPtr);
   }
   for (i=0; i<3; i++){
      intPtr=&result[i];
      pop(membank, intPtr);
   }
   printf("result[0]=%i\n", result[0]);
   printf("result[1]=%i\n", result[1]);
   printf("result[2]=%i\n", result[2]);
   printf("result[3]=%i\n", result[3]);

   printf("functioning");
}

